package com.kindgeek.utils;

import com.kindgeek.model.Student;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommonFileReaderTest_HappyPass {
    @Test
    void readFile() throws IOException {
        CommonFileReader<Student> reader = new CommonFileReader<>(Student.class, ",", true);
        List<Student> students = reader.read(getClass().getClassLoader().getResourceAsStream("students.txt"));

        System.out.println(Arrays.toString(students.toArray()));

        assertEquals(students.size(), 3);
        assertEquals(students.get(0), new Student("Student1", 8.5));
    }
}