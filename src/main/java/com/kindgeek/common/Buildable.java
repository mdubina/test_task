package com.kindgeek.common;

/**
 * This interface is used for mark classes that can be build from input line.
 * Line is represented as array of Strings.
 *
 * @param <T> returned class type.
 */
public interface Buildable<T> {
    /**
     * This method build new object of type T from input line.
     *
     * @param line input line. For example for Student{name=Sam,mark=10} line will look like: ["Sam", "10"].
     * @return
     */
    T build(String[] line);
}
