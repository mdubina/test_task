package com.kindgeek.model;

public class Result<T> {
    private long time;
    private T[] result;

    public Result(long time, T[] result) {
        this.time = time;
        this.result = result;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public T[] getResult() {
        return result;
    }

    public void setResult(T[] result) {
        this.result = result;
    }
}
