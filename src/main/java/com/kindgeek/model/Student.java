package com.kindgeek.model;

import com.kindgeek.common.Buildable;
import com.kindgeek.common.CommonRuntimeException;

import static java.lang.Double.compare;
import static java.lang.Double.parseDouble;

public class Student implements Buildable<Student>, Comparable<Student> {
    private static final int NUMBER_OF_FIELDS = 2;

    private String name;
    private double mark;

    public Student() {
    }

    public Student(String name, double mark) {
        this.name = name;
        this.mark = mark;
    }

    @Override
    public Student build(String[] line) {
        if (line.length == NUMBER_OF_FIELDS)
            return new Student(line[0], parseDouble(line[1]));

        //TODO: log.warn(...);
        throw new CommonRuntimeException("Unsupported line format");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", mark=" + mark +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (compare(student.mark, mark) != 0) return false;
        return name != null ? name.equals(student.name) : student.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public int compareTo(Student o) {
        return compare(mark, o.mark);
    }
}
