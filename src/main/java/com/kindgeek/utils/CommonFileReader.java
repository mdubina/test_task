package com.kindgeek.utils;

import com.kindgeek.common.Buildable;
import com.kindgeek.common.CommonRuntimeException;
import com.sun.istack.internal.Nullable;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CommonFileReader<T extends Buildable<T>> {
    public static final String DELIMITER = ",";

    private final String delimiter;
    private final boolean skipEmpty;
    private final Class<T> clazz;

    public CommonFileReader(Class<T> clazz) {
        this(clazz, DELIMITER, true);
    }

    public CommonFileReader(Class<T> clazz, String delimiter, boolean skipEmpty) {
        this.delimiter = delimiter;
        this.clazz = clazz;
        this.skipEmpty = skipEmpty;
    }

    public List<T> read(InputStream stream) throws IOException {
        List<T> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            String line = reader.readLine();
            while (line != null) {
                T t = readObject(line);
                if (t != null)
                    list.add(t);
                line = reader.readLine();
            }
        }

        return list;
    }

    public List<T> readFile(String file) throws IOException {
        return read(new FileInputStream(new File(file)));
    }

    @Nullable
    private T readObject(String line) {
        try {
            String[] strings = line.split(delimiter);
            if (strings.length == 0 && skipEmpty)
                return null;

            return clazz.newInstance().build(strings);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new CommonRuntimeException("Can't instantiate object.", e);
        }
    }
}
