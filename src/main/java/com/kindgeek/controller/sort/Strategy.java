package com.kindgeek.controller.sort;

public enum Strategy {
    BUBBLE,
    HEAP,
    MERGE
}
