package com.kindgeek.controller.sort;

public class BubbleSortStrategy implements SortingStrategy {
    @Override
    public void sort(Comparable[] array) {
        boolean swap;
        int end = array.length - 1;
        for (int i = 1; i <= end; i++) {
            swap = false;
            for (int j = 0; j <= end - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    Comparable temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    swap = true;
                }
            }
            if (!swap) {
                break;
            }
        }
    }
}
