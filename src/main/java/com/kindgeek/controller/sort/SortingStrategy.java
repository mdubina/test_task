package com.kindgeek.controller.sort;

public interface SortingStrategy {
    void sort(Comparable[] arr);
}
