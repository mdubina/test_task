package com.kindgeek.controller.sort;

public class MergeSortStrategy implements SortingStrategy {
    public void sort(Comparable[] array) {
        mergeSort(array, 0, array.length - 1);
    }

    private void mergeSort(Comparable[] array, int start, int end) {
        if (start < end) {
            int half = (start + end) / 2;
            mergeSort(array, start, half);
            mergeSort(array, half + 1, end);
            merge(array, start, half, end);
        }
    }

    private static void merge(Comparable[] array, int start, int half, int end) {
        Comparable[] temp = new Comparable[array.length];
        int start1 = start;
        int start2 = half + 1;
        int i = start1;
        while ((start1 <= half) && (start2 <= end)) {
            if (array[start1].compareTo(array[start2]) < 0) {
                temp[i] = array[start1];
                i = i + 1;
                start1 = start1 + 1;
            } else {
                temp[i] = array[start2];
                i = i + 1;
                start2 = start2 + 1;
            }
        }
        while (start1 <= half) {
            temp[i] = array[start1];
            i = i + 1;
            start1 = start1 + 1;
        }
        while (start2 <= end) {
            temp[i] = array[start2];
            i = i + 1;
            start2 = start2 + 1;
        }
        i = start;
        while (i <= end) {
            array[i] = temp[i];
            i = i + 1;
        }
    }
}
