package com.kindgeek.controller;

import com.kindgeek.controller.sort.*;
import com.kindgeek.model.Result;
import com.kindgeek.model.Student;
import com.kindgeek.utils.CommonFileReader;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.System.currentTimeMillis;
import static java.util.Arrays.stream;

public class Controller {
    private String fileName = "";
    private Strategy currentStrategy = Strategy.HEAP;
    private Result<Student> lastResult;

    private CommonFileReader<Student> reader;
    private List<Student> notSortedStudents;
    private Map<Strategy, SortingStrategy> strategies = new HashMap<>();

    public Controller() {
        reader = new CommonFileReader<>(Student.class, ",", true);

        strategies.put(Strategy.HEAP, new HeapStrategy());
        strategies.put(Strategy.BUBBLE, new BubbleSortStrategy());
        strategies.put(Strategy.MERGE, new MergeSortStrategy());
    }

    public List<Student> loadFile(String fileName) throws IOException {
        if (this.fileName.equals(fileName)) {
            return notSortedStudents;
        }
        notSortedStudents = reader.readFile(fileName);
        return notSortedStudents;
    }

    public void pickStrategy(Strategy strategy) {
        currentStrategy = strategy;
    }

    public Result<Student> sortStudents() {
        long time = currentTimeMillis();
        Student[] toSort = notSortedStudents.toArray(new Student[notSortedStudents.size()]);

        strategies.get(currentStrategy).sort(toSort);
        lastResult = new Result<>(currentTimeMillis() - time, toSort);
        return lastResult;
    }

    public void saveResult(File file) throws IOException {
        if (lastResult == null)
            return;

        PrintWriter writer = new PrintWriter(file);
        stream(lastResult.getResult()).forEach(s -> writer.println(s.getName() + "," + s.getMark()));
        writer.close();
    }
}
