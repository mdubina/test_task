package com.kindgeek.view;

import com.kindgeek.controller.Controller;
import com.kindgeek.controller.sort.Strategy;
import com.kindgeek.model.Result;
import com.kindgeek.model.Student;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.kindgeek.controller.sort.Strategy.BUBBLE;
import static com.kindgeek.controller.sort.Strategy.HEAP;
import static com.kindgeek.controller.sort.Strategy.MERGE;

public class MainFrame extends JFrame {
    private JLabel fileLevel = new JLabel("File: <not-loaded>");
    private static final String PATTERN = "Processed: %d records. Total time: %d ms.";

    private JLabel infoSection = new JLabel(formatInfo(0, 0));
    private JList studentsList;
    private boolean isFileLoaded;
    private Controller controller;

    public MainFrame(Controller controller) {
        this.controller = controller;
        setUpMainFrame();

        setUpMainPanel(controller);
    }

    private void setUpMainPanel(Controller controller) {
        JPanel panel = new JPanel(new BorderLayout());
        JPanel panelButtons = setUpButtonPanel(controller);
        JScrollPane pane = setUpListView();

        panel.add(fileLevel, BorderLayout.NORTH);
        panel.add(infoSection, BorderLayout.SOUTH);
        panel.add(panelButtons);
        panel.add(pane, BorderLayout.EAST);

        add(panel);
    }

    private JPanel setUpButtonPanel(Controller controller) {
        JPanel panelButtons = new JPanel(new FlowLayout());
        JButton btnLoad = setUpLoadButton(controller);
        JButton btnSort = setUpSortButton(controller);
        JButton btnSave = setUpSaveButton(controller);
        JComboBox<Strategy> comboBox = setUpStrategyComboBox(controller);

        panelButtons.add(btnLoad);
        panelButtons.add(comboBox);
        panelButtons.add(btnSort);
        panelButtons.add(btnSave);
        return panelButtons;
    }

    private void setUpMainFrame() {
        setTitle("Test.");
        setSize(450, 700);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private JComboBox<Strategy> setUpStrategyComboBox(Controller controller) {
        Strategy[] strategiesList = {HEAP, BUBBLE, MERGE};
        JComboBox<Strategy> comboBox = new JComboBox<>(strategiesList);
        comboBox.addActionListener(e -> controller.pickStrategy((Strategy) comboBox.getSelectedItem()));
        return comboBox;
    }

    private JScrollPane setUpListView() {
        studentsList = new JList();
        return new JScrollPane(studentsList);
    }

    private JButton setUpSaveButton(Controller controller) {
        JButton button = new JButton("Save");
        button.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();

            try {
                chooser.showOpenDialog(this);
                controller.saveResult(chooser.getSelectedFile());
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(this, "Can't save file.");
            }
        });

        return button;
    }

    private JButton setUpLoadButton(Controller controller) {
        JButton btnLoad = new JButton("Load");
        btnLoad.addActionListener(e -> {
            isFileLoaded = true;
            JFileChooser chooser = new JFileChooser();
            chooser.showOpenDialog(this);
            File file = chooser.getSelectedFile();
            try {
                List<Student> students = controller.loadFile(file.getAbsolutePath());
                fileLevel.setText("File: " + file.getName() + " is loaded.");

                studentsList.setListData(students.toArray());
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(this, "Can't load file");
            }
        });
        return btnLoad;
    }

    private JButton setUpSortButton(Controller controller) {
        JButton btnSort = new JButton("Sort");
        btnSort.addActionListener(e -> {
            if (!isFileLoaded) {
                JOptionPane.showMessageDialog(this, "File isn't loaded yet.");
                return;
            }
            Result<Student> result = controller.sortStudents();
            infoSection.setText(formatInfo(result.getResult().length, result.getTime()));
            studentsList.setListData(result.getResult());

        });
        return btnSort;
    }

    private String formatInfo(int size, long time) {
        return String.format(PATTERN, size, time);
    }
}