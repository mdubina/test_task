package com.kindgeek;

import com.kindgeek.controller.Controller;
import com.kindgeek.view.MainFrame;

import static java.awt.EventQueue.invokeLater;

public class Main {
    public static void main(String[] args) {
        invokeLater(() -> new MainFrame(new Controller()).setVisible(true));
    }
}
